﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ClientSideForChat
{
    public static class MessageColors
    {
        private static readonly Brush Black = Brushes.Black;
        private static readonly Brush Red = Brushes.Red;
        private static readonly Brush Blue = Brushes.Blue;
        private static readonly Brush Green = Brushes.Green;

        public static Brush ParseColor(string clr)
        {
            switch(clr)
            {
                case "Black" :  return Black; 
                case "Blue"  :  return Blue; 
                case "Red"   :  return Red; 
                case "Green" :  return Green;
                default: return Blue;
            }

         }
    }
}
