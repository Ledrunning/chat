﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientSideForChat
{
    public static class ConnectionSettings
    {
        public static string _host { get; set; }
        public static int _port { get; set; }
        public static readonly uint BUFFER = 2048;
    }
}
