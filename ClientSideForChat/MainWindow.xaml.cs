﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.IO;
using Microsoft.Win32;
using System.Windows.Media;
using static ClientSideForChat.ConnectionSettings;
using System.Windows.Documents;

namespace ClientSideForChat
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private delegate void ChatEvent(string content, string clr);
        private ChatEvent _addMessage;
        private Socket _serverSocket;
        private Thread listenThread;
       

        public MainWindow()
        {
            InitializeComponent();
          
            _addMessage = new ChatEvent(AddMessage);
           
            PrivateMessageItem.Click += delegate
            {
                if (userList.SelectedItems.Count > 0)
                {
                    messageData.Text = $"\"{userList.SelectedItem} ";
                }
            };

            SendFileItem.Click += delegate
            {
                if (userList.SelectedItems.Count == 0)
                {
                    return;
                }
                OpenFileDialog ofp = new OpenFileDialog();
                ofp.ShowDialog();
                if (!File.Exists(ofp.FileName))
                {
                    MessageBox.Show($"File {ofp.FileName} not found!");
                    return;
                }
                FileInfo fi = new FileInfo(ofp.FileName);
                byte[] buffer = File.ReadAllBytes(ofp.FileName);
                Send($"#sendfileto|{userList.SelectedItem}|{buffer.Length}|{fi.Name}");
                Send(buffer);


            };
            
            userList.ContextMenu = userMenu;

        }

        private void AddMessage(string chatMsgContent, string clr)
        {
            Brush msgClr = MessageColors.ParseColor(clr);
            var para = (Paragraph)chatBox.Document.Blocks.FirstBlock;

            if (!Dispatcher.CheckAccess()) // InvokeRequired
            {
                Dispatcher.Invoke(_addMessage, chatMsgContent, clr);
               //Dispatcher.Invoke(() => AddMessage(chatMsgContent));
                return;
            }

            AddLine(para, chatMsgContent, msgClr);
        }


        void AddLine(Paragraph para, string chatMsgContent, Brush color)
        {
            para.Inlines.Add(new Run(chatMsgContent) { Foreground = color });
            para.Inlines.Add(new LineBreak());
        }


        public void Send(byte[] buffer)
        {
            try
            {
                _serverSocket.Send(buffer);
            }
            catch { }
        }

        public void Send(string Buffer)
        {
            try
            {
                _serverSocket.Send(Encoding.Unicode.GetBytes(Buffer));
            }
            catch { }
        }


        public void handleCommand(string cmd)
        {

            string[] commands = cmd.Split('#');
            int countCommands = commands.Length;
            for (int i = 0; i < countCommands; i++)
            {
                try
                {
                    string currentCommand = commands[i];

                    if (string.IsNullOrEmpty(currentCommand))
                        continue;

                    if (currentCommand.Contains("setnamesuccess"))
                    {
       
                        Dispatcher.BeginInvoke(new Action(delegate
                        {
                            AddMessage($"Welcome, {nicknameData.Text}", "Green");
                            nameData.Content = nicknameData.Text;
                            chatBox.IsEnabled = true;
                            messageData.IsEnabled = true;
                            userList.IsEnabled = true;
                            nicknameData.IsEnabled = false;
                            //enterChat.IsEnabled = true;
                        }));

                        continue;
                    }
                    if (currentCommand.Contains("setnamefailed"))
                    {
                        AddMessage("Incorect Nickname!", "Red");
                        continue;
                    }
                    if (currentCommand.Contains("msg"))
                    {
                        string[] Arguments = currentCommand.Split('|');
                        AddMessage(Arguments[1], Arguments[2]);
                        continue;
                    }

                    if (currentCommand.Contains("userlist"))
                    {
                        char[] charSeparators = new char[] { ',' };
                        string[] Users = currentCommand.Split('|')[1].Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                        int countUsers = Users.Length;

                        userList.Dispatcher.BeginInvoke(new Action(delegate { userList.Items.Clear(); }));
                      
                        for (int j = 0; j < countUsers; j++)
                        {
                            userList.Dispatcher.BeginInvoke (new Action (delegate { userList.Items.Add(Users[j-1]); }));
                        }
                        continue;

                    }

                    if (currentCommand.Contains("gfile"))
                    {
                        string[] Arguments = currentCommand.Split('|');
                        string fileName = Arguments[1];
                        string FromName = Arguments[2];
                        string FileSize = Arguments[3];
                        string idFile = Arguments[4];
                       
                        MessageBox.Show($"Are you sure to recieve this file {fileName} with size {FileSize} from {FromName}", "File", MessageBoxButton.YesNo);

                        if (DialogResult.Value == true)
                        {
                            Thread.Sleep(1000);
                            Send("#yy|" + idFile);
                            byte[] fileBuffer = new byte[int.Parse(FileSize)];
                            _serverSocket.Receive(fileBuffer);
                            File.WriteAllBytes(fileName, fileBuffer);
                            MessageBox.Show($"File {fileName} has been recieved.");
                        }
                        else
                            Send("nn");
                        continue;
                    }

                }
                catch (Exception exp)
                {
                    Console.WriteLine("Error with handleCommand: " + exp.Message);
                }

            }


        }
        public void listner()
        {
            try
            {
                while (_serverSocket.Connected)
                {
                    byte[] buffer = new byte[BUFFER];
                    int bytesReceive = _serverSocket.Receive(buffer);
                    handleCommand(Encoding.Unicode.GetString(buffer, 0, bytesReceive));
                }
            }
            catch
            {
                MessageBox.Show("Connection with server is lost");
                _serverSocket.Dispose();
            }
        }

           
        private void messageData_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string msgData = messageData.Text;
                if (string.IsNullOrEmpty(msgData))
                    return;
                if (msgData[0] == '"')
                {
                    string temp = msgData.Split(' ')[0];
                    string content = msgData.Substring(temp.Length + 1);
                    temp = temp.Replace("\"", string.Empty);
                    Send($"#private|{temp}|{content}");
                }
                else
                    Send($"#message|{msgData}");
                messageData.Text = string.Empty;
            }
        }

        private void Chat_WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_serverSocket.Connected)
                Send("#endsession");
            _serverSocket.Dispose();
        }

        // Disconecting now! It's not necessery;

        private void ChatWindows_Load(object sender, RoutedEventArgs e)
        {
            
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SendMsg_Click(object sender, RoutedEventArgs e)
        {
            string msgData = messageData.Text;

            if (string.IsNullOrEmpty(msgData))
                return;

            if (msgData[0] == '"')
            {
                string temp = msgData.Split(' ')[0];
                string content = msgData.Substring(temp.Length + 1);
                temp = temp.Replace("\"", string.Empty);
                Send($"#private|{temp}|{content}");
            }
            else
                Send($"#message|{msgData}");
            messageData.Text = string.Empty;
           
        }

        private void enterChat_Click(object sender, RoutedEventArgs e)
        {
            int res;
            bool isInt = Int32.TryParse(serverPort.Text, out res);
            string nickName = nicknameData.Text;
            nameData.Content = nickName;

            if (isInt)
            {
                _port = res;
            }
            _host = serverIP.Text;

            if (string.IsNullOrEmpty(nickName))
            {
                MessageBox.Show("Enter Nick name! ", "Warning!",
                                MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
 
            try
            {
                IPAddress temp = IPAddress.Parse(_host);
                _serverSocket = new Socket(temp.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                _serverSocket.Connect(new IPEndPoint(temp, _port));
                Send($"#setname|{nickName}");

                if (_serverSocket.Connected)
                {
   
                    Dispatcher.BeginInvoke(new Action(delegate
                    {
                        enterChat.IsEnabled = false;
                        nicknameData.IsEnabled = true;
                        AddMessage("Connection with server is established.", "Green");
                    }));

                    listenThread = new Thread(listner);
                    listenThread.IsBackground = true;
                    listenThread.Start();
                }
                else
  
                    AddMessage("Connection with server is not established.", "Red");
                

               
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(serverIP.Text) || string.IsNullOrEmpty(serverPort.Text))
                {
                    MessageBox.Show("IP port or Hostname is empty! " + "Enter correct data! ", "Error!",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show(ex.Message + "Enter correct data! ", "Error!",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                    serverPort.Text = string.Empty;
                    serverIP.Text = string.Empty;
                }
  
            }

        }

        private void localhost_Checked(object sender, RoutedEventArgs e)
        {
            _port = 2222;
            _host = "127.0.0.1";
            serverIP.Text = _host;
            serverPort.Text = _port.ToString();
        }

        private void disconectChat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_serverSocket.Connected)
                {
                    Send("#endsession");
                    Dispatcher.Invoke(new Action(delegate
                    {
                        enterChat.IsEnabled = true;
                        nicknameData.IsEnabled = true;
                       // chatBox.Document.Blocks.FirstBlock;
                       //
                        _serverSocket.Dispose();
                    }));
  
                }
            }
            catch
            {
                MessageBox.Show("Connection unavailable!", "Warning!",
                                       MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
    }
}