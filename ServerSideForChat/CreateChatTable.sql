﻿
CREATE TABLE [dbo].[ChatContent] 
(
	Id int PRIMARY KEY NOT NULL IDENTITY(1,1), 
	DateAndTime DateTimeOffset (3) NOT NULL, 
	UserName nvarchar (50) NOT NULL ,
	ChatMessages nvarchar(1000) NOT NULL 
);
