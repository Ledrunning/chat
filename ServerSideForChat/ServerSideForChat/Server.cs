﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using static ServerSideForChat.ServerSettings;

namespace ServerSideForChat
{
    public static class Server
    {
        public struct FileD
        {
            public int id;
            public string fileName;
            public string from;
            public int fileSize;
            public byte[] fileBuffer;
        }

        public static List<FileD> Files = new List<FileD>();

        public delegate void UserEvent(string name);

        public static int countUsers = 0;
 
        public static event UserEvent UserConnected = (Username) =>
        {
            Console.WriteLine($"User {Username} connected.");
            countUsers++;
            SendGlobalMessage($"User {Username} conected to chat.", "Green"); 
            SendUserList();
        };

        public static event UserEvent UserDisconnected = (Username) =>
        {
            Console.WriteLine($"User {Username} disconnected.");
            countUsers--;
            SendGlobalMessage($"User {Username} disconnected from chat.", "Green"); 
            SendUserList();
        };

        public static List<User> UserList = new List<User>();
        public static Socket serverSocket;

        public static FileD GetFileByID(int ID)
        {
            int countFiles = Files.Count;
            for (int i = 0; i < countFiles; i++)
            {
                if (Files[i].id == ID)
                    return Files[i];
            }
            return new FileD() { id = 0 };
        }

        public static void NewUser(User usr)
        {
            if (UserList.Contains(usr))
                return;
            UserList.Add(usr);
            UserConnected(usr.Username);
        }

        public static void EndUser(User usr)
        {
            if (!UserList.Contains(usr))
                return;
            UserList.Remove(usr);
            usr.End();
            UserDisconnected(usr.Username);

        }

        public static User GetUser(string Name)
        {
            for (int i = 0; i < countUsers; i++)
            {
                if (UserList[i].Username == Name)
                    return UserList[i];
            }
            return null;
        }

        public static void SendUserList()
        {
            string userList = "#userlist|";

            for (int i = 0; i < countUsers; i++)
            {
                userList += UserList[i].Username + ",";
            }

            SendAllUsers(userList);
        }

        public static void SendGlobalMessage(string content, string clr)
        {
            for (int i = 0; i < countUsers; i++)
            {
                UserList[i].SendMessage(content, clr);
            }
        }

        public static void SendAllUsers(byte[] data)
        {
            for (int i = 0; i < countUsers; i++)
            {
                UserList[i].Send(data);
            }
        }

        public static void SendAllUsers(string data)
        {
            for (int i = 0; i < countUsers; i++)
            {
                UserList[i].Send(data);
            }
        }


    }
}
