﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ServerSideForChat
{
    public class User
    {
        private Thread _userThread;
        private string _nickName;
        private bool authSuccess = false;
        private readonly UInt32 SIZE = 2048;
        public string Username
        {
            get { return _nickName; }
        }
        private Socket _userHandle;

        public User(Socket handle)
        {
            _userHandle = handle;
            _userThread = new Thread(Listener);
            _userThread.IsBackground = true;
            _userThread.Start();
        }

        private void Listener()
        {
            try
            {
                while(_userHandle.Connected)
                {
                    byte[] buffer = new byte[SIZE];
                    int bytesRecieve = _userHandle.Receive(buffer);
                    HandleCommand(Encoding.Unicode.GetString(buffer, 0, bytesRecieve));
                }
            }

            catch
            {
                Server.EndUser(this);
            }
        }

        private bool SetName(string Name)
        {
            _nickName = Name;
            Server.NewUser(this);
            authSuccess = true;
            return true;
        }

        private void HandleCommand(string cmd)
        {
            try
            {
                string[] commands = cmd.Split('#');
                int countCommands = commands.Length;

                for (int i = 0; i < countCommands; i++)
                {
                    string currentCommand = commands[i];

                    if (string.IsNullOrEmpty(currentCommand))
                    {
                        continue;
                    }

                    if (!authSuccess)
                    {
                        if (currentCommand.Contains("setname"))
                        {
                            if (SetName(currentCommand.Split('|')[1]))
                                Send("#setnamesuccess");
                            else
                                Send("#setnamefailed");
                        }
                        continue;
                    }

                    if (currentCommand.Contains("yy"))
                    {
                        string id = currentCommand.Split('|')[1];
                        Server.FileD file = Server.GetFileByID(int.Parse(id));
                        if (file.id == 0)
                        {
                            SendMessage("File transmition error...", "Red");
                            continue;
                        }
                        Send(file.fileBuffer);
                        Server.Files.Remove(file);
                    }

                    if (currentCommand.Contains("message")) 
                    {
                        string[] Arguments = currentCommand.Split('|');
                        Server.SendGlobalMessage($"[{_nickName}]: {Arguments[1]}", "Blue"); 
                        Console.WriteLine($"[{_nickName}]: {Arguments[1]}");

                        continue;
                    }
                    
                    ///
                    /// SendFiles
                    //

                    if (currentCommand.Contains("endsession"))
                    {
                        Server.EndUser(this);
                        return;
                    }
                    if (currentCommand.Contains("sendfileto"))
                    {
                        string[] arguments = currentCommand.Split('|');
                        string targetName = arguments[1];
                        int fileSize = int.Parse(arguments[2]);
                        string FileName = arguments[3];
                        byte[] fileBuffer = new byte[fileSize];
                        _userHandle.Receive(fileBuffer);
                        User targetUser = Server.GetUser(targetName);

                        if (targetUser == null)
                        {
                            SendMessage($"User {FileName} not found!", "Red");
                            continue;
                        }

                        Server.FileD newFile = new Server.FileD()
                        {
                            id = Server.Files.Count + 1,
                            fileName = FileName,
                            from = Username,
                            fileBuffer = fileBuffer,
                            fileSize = fileBuffer.Length
                        };
                        Server.Files.Add(newFile);
                        targetUser.SendFile(newFile, targetUser);
                    }
                    if (currentCommand.Contains("private"))
                    {
                        string[] Arguments = currentCommand.Split('|');
                        string TargetName = Arguments[1];
                        string Content = Arguments[2];
                        User targetUser = Server.GetUser(TargetName);
                        if (targetUser == null)
                        {
                            SendMessage($"User {TargetName} not found!", "Red");
                            continue;
                        }
                        SendMessage($"-[Sent][{TargetName}]: {Content}", "Black");
                        targetUser.SendMessage($"-[Recieved][{Username}]: {Content}", "Black");
                        continue;
                    }

                }

            }
            catch (Exception exp)
            {
                Console.WriteLine("Error with handleCommand: " + exp.Message);
            }

        }


        public void SendFile(Server.FileD fd, User To)
        {
            byte[] answerBuffer = new byte[48];
            Console.WriteLine($"Sending {fd.fileName} from {fd.from} to {To.Username}");
            To.Send($"#gfile|{fd.fileName}|{fd.from}|{fd.fileBuffer.Length}|{fd.id}");
        }

        public void SendMessage(string content, string clr)
        {
            Send($"#msg|{content}|{clr}"); 
        }

        public void Send(byte[] buffer)
        {
            try
            {
                _userHandle.Send(buffer);
            }
            catch(Exception e)
            {
                Console.WriteLine("Error to send! " + e.Message);
            }
        }

        public void Send(string Buffer)
        {
            try
            {
                _userHandle.Send(Encoding.Unicode.GetBytes(Buffer));
            }
            catch(Exception e)
            {
                Console.WriteLine("Error to send! " + e.Message);
            }
        }

        public void End()
        {
            try
            {
                _userHandle.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine("Error to data transmission " + e.Message);
            }

        }
    }
}
