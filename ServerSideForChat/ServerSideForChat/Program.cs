﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using static ServerSideForChat.ServerSettings;

namespace ServerSideForChat
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Server";
            Console.ForegroundColor = ConsoleColor.Green;
            Loading spin = new Loading();

            IPAddress address = IPAddress.Parse(HOST);
            Server.serverSocket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            Server.serverSocket.Bind(new IPEndPoint(address, PORT));
            Server.serverSocket.Listen(10);
            Console.Write(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " ");
            Console.WriteLine($"Server has been started on { HOST }:{PORT}");
            Console.WriteLine("Waiting connections...");

            while (work)
            {
                bool s = Server.serverSocket.Connected;
                Socket handle = Server.serverSocket.Accept();
                Console.WriteLine($"New connection:  {handle.RemoteEndPoint.ToString()} {DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " "}");
                new User(handle);
            }

            Console.WriteLine("Server closing...");
        } 
    }
}
