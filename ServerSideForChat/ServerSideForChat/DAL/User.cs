﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSideForChat.DAL
{
    public class User
    {
        public int Id { get; set; }
        public string DateAndTime { get; set; }
        public string UserName { get; set; }
        public string ChatMessages { get; set; }

        public User() { }

        public User(int id, string date, string userName, string chatMsg)
        {
            Id = id;
            DateAndTime = date;
            UserName = userName;
            ChatMessages = chatMsg;
        }

        public User(string date, string userName, string chatMsg)
        {
            DateAndTime = date;
            UserName = userName;
            ChatMessages = chatMsg;
        }
    }
}
