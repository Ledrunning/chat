﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSideForChat.DAL
{
    public static class DBConnectionManager
    {
        static string conStr = ConfigurationManager.ConnectionStrings["connectionToChatDB"].ConnectionString;
        public static event EventHandler Changed;

        static DBConnectionManager() { }

        public static List<User> ReadData()
        {
            using (SqlConnection con = new SqlConnection(conStr))
            {
                string select = "SELECT * FROM ChatContent";
                SqlCommand cmd = new SqlCommand(select, con);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<User> tableList = new List<User>();

                while (reader.Read())
                {
                    tableList.Add(new User(Convert.ToInt32(reader["Id"]), reader["DateAndTime"].ToString(),
                                          reader["UserName"].ToString(), reader["ChatMessages"].ToString()));
                }

                con.Close();
                return tableList;
            }

        }

        public static bool IsFull()
        {
            using (SqlConnection con = new SqlConnection(conStr))
            {
                string selectRequest = "SELECT * FROM ChatContent";
                SqlCommand cmd = new SqlCommand(selectRequest, con);
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<User> tableList = new List<User>();
                while (reader.Read())
                {
                    tableList.Add(new User(Convert.ToInt32(reader["Id"]), reader["DateAndTime"].ToString(),
                                          reader["UserName"].ToString(), reader["ChatMessages"].ToString()));
                }

                if (tableList != null && tableList.Count > 0)
                    return true;
                return false;
            }
        }

        public static void InsertData(User u)
        {

            using (SqlConnection con = new SqlConnection(conStr))
            {
                // string insert = String.Format("INSERT INTO Student (Name, SecondName, DepartmentId) VALUES (N'{0}', N'{1}', N'{2}')", n, sn, di);
                string insert = "INSERT INTO Student (Name, SecondName, DepartmentId) VALUES (@Name, @SecondName, @DepartmentId)";
                SqlCommand cmd = new SqlCommand(insert, con);
                // Защищаем от ввода SQL комманд;
                SqlParameter parametrDate = new SqlParameter("Name", u.DateAndTime);
                SqlParameter parametrUserName = new SqlParameter("SecondName", u.UserName);
                SqlParameter ChatMessages = new SqlParameter("DepartmentId", u.ChatMessages);

                cmd.Parameters.Add(parametrDate);
                cmd.Parameters.Add(parametrUserName);
                cmd.Parameters.Add(ChatMessages);

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    
                }
                finally
                {
                    con.Close();
                }

                if (Changed != null)  // В новой студии можно просто Changed??.Invoke;
                {
                    Changed.Invoke(null, new EventArgs());
                }
            }

        }


    }
}
