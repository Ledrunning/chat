﻿using System;
using System.Threading;

namespace ServerSideForChat
{
    class Loading
    {
        int counter;
        public Loading()
        {
            counter = 0;
        }
        public void Turn(bool s)
        {
            while (!s)
            {
                counter++;
                switch (counter % 4)
                {
                    case 0: Console.Write("/"); Thread.Sleep(200); break;
                    case 1: Console.Write("-"); Thread.Sleep(200); break;
                    case 2: Console.Write("\\"); Thread.Sleep(200); break;
                    case 3: Console.Write("|"); Thread.Sleep(200); break;
                }
                Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
            }
        }
    }
}
