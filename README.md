# README #

### STUDY PROJECT FOR IT STEP COURSES ###

* Quick summary: Multi-user server-client TCP chat written on C# WPF
* Version: 0.1b

![Alt text](https://habrastorage.org/web/2d4/66b/9c4/2d466b9c432e4977a33dd0a0292f1b65.png)

### Contribution guidelines ###

* Test application;
* Writing tests;
* Code review;

### Who do I talk to? ###

* Students, .NET interns;
* Cyberforum community;